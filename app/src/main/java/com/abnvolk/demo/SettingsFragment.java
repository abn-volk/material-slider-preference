package com.abnvolk.demo;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import com.abnvolk.preference.slider.FloatMaterialSliderPreference;

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.prefs);

        ((FloatMaterialSliderPreference) findPreference("slider_2c")).setLabelFormatter(value -> "Value: " + value);
    }
}
