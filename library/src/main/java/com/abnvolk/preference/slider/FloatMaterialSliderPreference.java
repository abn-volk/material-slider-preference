package com.abnvolk.preference.slider;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;

import com.google.android.material.slider.LabelFormatter;
import com.google.android.material.slider.Slider;


/**
 * Preference based on android.preference.SeekBarPreference but uses support preference as a base
 * . It contains a title and a {@link SeekBar} and an optional SeekBar value {@link TextView}.
 * The actual preference layout is customizable by setting {@code android:layout} on the
 * preference widget layout or {@code seekBarPreferenceStyle} attribute.
 *
 * <p>The {@link SeekBar} within the preference can be defined adjustable or not by setting {@code
 * adjustable} attribute. If adjustable, the preference will be responsive to DPAD left/right keys.
 * Otherwise, it skips those keys.
 *
 * <p>The {@link SeekBar} value view can be shown or disabled by setting {@code showSeekBarValue}
 * attribute to true or false, respectively.
 *
 * <p>Other {@link SeekBar} specific attributes (e.g. {@code title, summary, defaultValue, min,
 * max})
 * can be set directly on the preference widget layout.
 */
public class FloatMaterialSliderPreference extends Preference {
    private static final String TAG = "SliderPreference";
    private float mSeekBarValue;
    private float mMin;
    private float mMax;
    private float mSeekBarIncrement;
    private boolean mTrackingTouch;
    private Slider mSeekBar;
    private TextView mSeekBarValueTextView;
    // Whether the SeekBar should respond to the left/right keys
    private boolean mAdjustable;
    // Whether to show the SeekBar value TextView next to the bar
    private boolean mShowSeekBarValue;
    // Whether the SeekBarPreference should continuously save the Seekbar value while it is being
    // dragged.
    private boolean mUpdatesContinuously;
    private final int labelBehavior;
    private final boolean tickVisible;
    private LabelFormatter labelFormatter;
    /**
     * Listener reacting to the {@link SeekBar} changing value by the user
     */
    private final Slider.OnChangeListener mSeekBarChangeListener = new Slider.OnChangeListener() {
        @Override
        public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
            if (fromUser && (mUpdatesContinuously || !mTrackingTouch)) {
                syncValueInternal(slider);
            } else {
                // We always want to update the text while the seekbar is being dragged
                updateLabelValue(value);
            }
        }
    };

    private final Slider.OnSliderTouchListener mTouchListener = new Slider.OnSliderTouchListener() {

        @Override
        public void onStartTrackingTouch(@NonNull Slider slider) {
            mTrackingTouch = true;
        }

        @Override
        public void onStopTrackingTouch(@NonNull Slider slider) {
            mTrackingTouch = false;
            if (slider.getValue() + mMin != mSeekBarValue) {
                syncValueInternal(slider);
            }
        }
    };
    /**
     * Listener reacting to the user pressing DPAD left/right keys if {@code
     * adjustable} attribute is set to true; it transfers the key presses to the {@link SeekBar}
     * to be handled accordingly.
     */
    private final View.OnKeyListener mSeekBarKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != KeyEvent.ACTION_DOWN) {
                return false;
            }
            if (!mAdjustable && (keyCode == KeyEvent.KEYCODE_DPAD_LEFT
                    || keyCode == KeyEvent.KEYCODE_DPAD_RIGHT)) {
                // Right or left keys are pressed when in non-adjustable mode; Skip the keys.
                return false;
            }
            // We don't want to propagate the click keys down to the SeekBar view since it will
            // create the ripple effect for the thumb.
            if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER) {
                return false;
            }
            if (mSeekBar == null) {
                Log.e(TAG, "SeekBar view is null and hence cannot be adjusted.");
                return false;
            }
            return mSeekBar.onKeyDown(keyCode, event);
        }
    };

    public FloatMaterialSliderPreference(
            Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray a = context.obtainStyledAttributes(
                attrs, R.styleable.FloatMaterialSliderPreference, defStyleAttr, defStyleRes);
        // The ordering of these two statements are important. If we want to set max first, we need
        // to perform the same steps by changing min/max to max/min as following:
        // mMax = a.getInt(...) and setMin(...).
        mMin = a.getFloat(R.styleable.FloatMaterialSliderPreference_valueFrom, 0);
        setValueTo(a.getFloat(R.styleable.FloatMaterialSliderPreference_valueTo, 100));
        setStepSize(a.getFloat(R.styleable.FloatMaterialSliderPreference_stepSize, 0));
        mAdjustable = a.getBoolean(R.styleable.FloatMaterialSliderPreference_adjustable, true);
        mShowSeekBarValue = a.getBoolean(R.styleable.FloatMaterialSliderPreference_showSeekBarValue, false);
        mUpdatesContinuously = a.getBoolean(R.styleable.FloatMaterialSliderPreference_updatesContinuously,
                false);
        labelBehavior = a.getInt(R.styleable.FloatMaterialSliderPreference_labelBehavior, LabelFormatter.LABEL_FLOATING);
        tickVisible = a.getBoolean(R.styleable.FloatMaterialSliderPreference_tickVisible, false);
        a.recycle();
    }
    public FloatMaterialSliderPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, R.style.Preference_FloatMaterialSliderPreferenceStyle);
    }
    public FloatMaterialSliderPreference(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.floatMaterialSliderPreferenceStyle);
    }
    public FloatMaterialSliderPreference(Context context) {
        this(context, null);
    }
    @Override
    public void onBindViewHolder(PreferenceViewHolder view) {
        super.onBindViewHolder(view);
        view.itemView.setOnKeyListener(mSeekBarKeyListener);
        mSeekBar = (Slider) view.findViewById(R.id.seekbar);
        mSeekBarValueTextView = (TextView) view.findViewById(R.id.seekbar_value);
        if (mShowSeekBarValue) {
            mSeekBarValueTextView.setVisibility(View.VISIBLE);
        } else {
            mSeekBarValueTextView.setVisibility(View.GONE);
            mSeekBarValueTextView = null;
        }
        if (mSeekBar == null) {
            Log.e(TAG, "SeekBar view is null in onBindViewHolder.");
            return;
        }
        mSeekBar.clearOnChangeListeners();
        mSeekBar.clearOnSliderTouchListeners();
        mSeekBar.addOnChangeListener(mSeekBarChangeListener);
        mSeekBar.addOnSliderTouchListener(mTouchListener);
        mSeekBar.setValueFrom(mMin);
        mSeekBar.setValueTo(mMax);
        // If the increment is not zero, use that. Otherwise, use the default mKeyProgressIncrement
        // in AbsSeekBar when it's zero. This default increment value is set by AbsSeekBar
        // after calling setMax. That's why it's important to call setKeyProgressIncrement after
        // calling setMax() since setMax() can change the increment value.
        if (mSeekBarIncrement != 0) {
            mSeekBar.setStepSize(mSeekBarIncrement);
        } else {
            mSeekBarIncrement = mSeekBar.getStepSize();
        }
        if (mSeekBarValue < mMin) {
            mSeekBarValue = mMin;
        }
        if (mSeekBarValue > mMax) {
            mSeekBarValue = mMax;
        }
        mSeekBar.setValue(mSeekBarValue);
        updateLabelValue(mSeekBarValue);
        mSeekBar.setEnabled(isEnabled());
        mSeekBar.setLabelBehavior(labelBehavior);
        mSeekBar.setTickVisible(tickVisible);
        mSeekBar.setLabelFormatter(labelFormatter);
    }
    @Override
    protected void onSetInitialValue(Object defaultValue) {
        if (defaultValue == null) {
            defaultValue = 0f;
        }
        setValue(getPersistedFloat((Float) defaultValue));
    }
    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getFloat(index, 0);
    }
    /**
     * Gets the lower bound set on the {@link SeekBar}.
     *
     * @return The lower bound set
     */
    public float getValueFrom() {
        return mMin;
    }
    /**
     * Sets the lower bound on the {@link SeekBar}.
     *
     * @param min The lower bound to set
     */
    public void setValueFrom(float min) {
        if (min > mMax) {
            min = mMax;
        }
        if (min != mMin) {
            mMin = min;
            notifyChanged();
        }
    }
    /**
     * Returns the amount of increment change via each arrow key click. This value is derived from
     * user's specified increment value if it's not zero. Otherwise, the default value is picked
     * from the default mKeyProgressIncrement value in {@link android.widget.AbsSeekBar}.
     *
     * @return The amount of increment on the {@link SeekBar} performed after each user's arrow
     * key press
     */
    public final float getStepSize() {
        return mSeekBarIncrement;
    }
    /**
     * Sets the increment amount on the {@link SeekBar} for each arrow key press.
     *
     * @param seekBarIncrement The amount to increment or decrement when the user presses an
     *                         arrow key.
     */
    public final void setStepSize(float seekBarIncrement) {
        if (seekBarIncrement != mSeekBarIncrement) {
            mSeekBarIncrement = Math.min(mMax - mMin, Math.abs(seekBarIncrement));
            notifyChanged();
        }
    }
    /**
     * Gets the upper bound set on the {@link SeekBar}.
     *
     * @return The upper bound set
     */
    public float getValueTo() {
        return mMax;
    }
    /**
     * Sets the upper bound on the {@link SeekBar}.
     *
     * @param max The upper bound to set
     */
    public final void setValueTo(float max) {
        if (max < mMin) {
            max = mMin;
        }
        if (max != mMax) {
            mMax = max;
            notifyChanged();
        }
    }
    /**
     * Gets whether the {@link SeekBar} should respond to the left/right keys.
     *
     * @return Whether the {@link SeekBar} should respond to the left/right keys
     */
    public boolean isAdjustable() {
        return mAdjustable;
    }
    /**
     * Sets whether the {@link SeekBar} should respond to the left/right keys.
     *
     * @param adjustable Whether the {@link SeekBar} should respond to the left/right keys
     */
    public void setAdjustable(boolean adjustable) {
        mAdjustable = adjustable;
    }
    /**
     * Gets whether the {@link FloatMaterialSliderPreference} should continuously save the {@link SeekBar} value
     * while it is being dragged. Note that when the value is true,
     * {@link OnPreferenceChangeListener} will be called continuously as well.
     *
     * @return Whether the {@link FloatMaterialSliderPreference} should continuously save the {@link SeekBar}
     * value while it is being dragged
     * @see #setUpdatesContinuously(boolean)
     */
    public boolean getUpdatesContinuously() {
        return mUpdatesContinuously;
    }
    /**
     * Sets whether the {@link FloatMaterialSliderPreference} should continuously save the {@link SeekBar} value
     * while it is being dragged.
     *
     * @param updatesContinuously Whether the {@link FloatMaterialSliderPreference} should continuously save
     *                            the {@link SeekBar} value while it is being dragged
     * @see #getUpdatesContinuously()
     */
    public void setUpdatesContinuously(boolean updatesContinuously) {
        mUpdatesContinuously = updatesContinuously;
    }
    /**
     * Gets whether the current {@link SeekBar} value is displayed to the user.
     *
     * @return Whether the current {@link SeekBar} value is displayed to the user
     * @see #setShowSeekBarValue(boolean)
     */
    public boolean getShowSeekBarValue() {
        return mShowSeekBarValue;
    }
    /**
     * Sets whether the current {@link SeekBar} value is displayed to the user.
     *
     * @param showSeekBarValue Whether the current {@link SeekBar} value is displayed to the user
     * @see #getShowSeekBarValue()
     */
    public void setShowSeekBarValue(boolean showSeekBarValue) {
        mShowSeekBarValue = showSeekBarValue;
        notifyChanged();
    }
    private void setValueInternal(float seekBarValue, boolean notifyChanged) {
        if (seekBarValue < mMin) {
            seekBarValue = mMin;
        }
        if (seekBarValue > mMax) {
            seekBarValue = mMax;
        }
        if (seekBarValue != mSeekBarValue) {
            mSeekBarValue = seekBarValue;
            updateLabelValue(mSeekBarValue);
            persistFloat(seekBarValue);
            if (notifyChanged) {
                notifyChanged();
            }
        }
    }
    /**
     * Gets the current progress of the {@link SeekBar}.
     *
     * @return The current progress of the {@link SeekBar}
     */
    public float getValue() {
        return mSeekBarValue;
    }
    /**
     * Sets the current progress of the {@link SeekBar}.
     *
     * @param seekBarValue The current progress of the {@link SeekBar}
     */
    public void setValue(float seekBarValue) {
        setValueInternal(seekBarValue, true);
    }

    public void setLabelFormatter(LabelFormatter formatter) {
        labelFormatter = formatter;
        if (mSeekBar != null) {
            mSeekBar.setLabelFormatter(formatter);
        }
    }

    /**
     * Persist the {@link SeekBar}'s SeekBar value if callChangeListener returns true, otherwise
     * set the {@link SeekBar}'s value to the stored value.
     */
    private void syncValueInternal(Slider seekBar) {
        float seekBarValue = seekBar.getValue();
        if (seekBarValue != mSeekBarValue) {
            if (callChangeListener(seekBarValue)) {
                setValueInternal(seekBarValue, false);
            } else {
                seekBar.setValue(mSeekBarValue);
                updateLabelValue(mSeekBarValue);
            }
        }
    }
    /**
     * Attempts to update the TextView label that displays the current value.
     *
     * @param value the value to display next to the {@link SeekBar}
     */
    private void updateLabelValue(float value) {
        if (mSeekBarValueTextView != null) {
            mSeekBarValueTextView.setText(String.valueOf(value));
        }
    }
    @Override
    protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        if (isPersistent()) {
            // No need to save instance state since it's persistent
            return superState;
        }
        // Save the instance state
        final SavedState myState = new SavedState(superState);
        myState.mSeekBarValue = mSeekBarValue;
        myState.mMin = mMin;
        myState.mMax = mMax;
        return myState;
    }
    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!state.getClass().equals(SavedState.class)) {
            // Didn't save state for us in onSaveInstanceState
            super.onRestoreInstanceState(state);
            return;
        }
        // Restore the instance state
        SavedState myState = (SavedState) state;
        super.onRestoreInstanceState(myState.getSuperState());
        mSeekBarValue = myState.mSeekBarValue;
        mMin = myState.mMin;
        mMax = myState.mMax;
        notifyChanged();
    }
    /**
     * SavedState, a subclass of {@link BaseSavedState}, will store the state of this preference.
     *
     * <p>It is important to always call through to super methods.
     */
    private static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR =
                new Creator<FloatMaterialSliderPreference.SavedState>() {
                    @Override
                    public FloatMaterialSliderPreference.SavedState createFromParcel(Parcel in) {
                        return new FloatMaterialSliderPreference.SavedState(in);
                    }
                    @Override
                    public FloatMaterialSliderPreference.SavedState[] newArray(int size) {
                        return new FloatMaterialSliderPreference.SavedState[size];
                    }
                };
        float mSeekBarValue;
        float mMin;
        float mMax;
        SavedState(Parcel source) {
            super(source);
            // Restore the click counter
            mSeekBarValue = source.readFloat();
            mMin = source.readFloat();
            mMax = source.readFloat();
        }
        SavedState(Parcelable superState) {
            super(superState);
        }
        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            // Save the click counter
            dest.writeFloat(mSeekBarValue);
            dest.writeFloat(mMin);
            dest.writeFloat(mMax);
        }
    }
}